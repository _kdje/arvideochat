﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using Byn.Media;
using UnityEngine;
using UnityEngine.Experimental.XR;
using UnityEngine.XR.ARFoundation;

namespace Byn.Unity.Examples
{
    
    /// <summary>
    /// This app is the same as the callapp just refreshing the
    /// video list later to give the virtual video input some time to
    /// setup.
    /// 
    /// See VirtualCamera for documentation.
    /// </summary>
    public class ARCallApp : CallApp
    {

        enum DrawingMode
        {
            NOTDRAWING,
            DRAWING
        }
        DrawingMode currentDrawingMode = DrawingMode.NOTDRAWING;
        List<Vector3> currentDrawingHits;
        List<GameObject> currentLineRenderers;
        GameObject currentLineRenderer;
        static List<ARRaycastHit> s_Hits = new List<ARRaycastHit>();
        List<GameObject> spawnedObjects;
        public ARSessionOrigin m_SessionOrigin;
        public GameObject m_PlacedPrefab;
        public GameObject lineRenderer2p;
        public List<LineRenderer> allDrawings;

        protected override void Start()
        {
            base.Start();
#if UNITY_WEBGL
        //doesn't work with WebGL
        Debug.LogError("VideoInput isn't supported in the browser.");
#else
            //need to fresh the ui a bit later as the virtual input needs a while to start
            //without this the virtual camera wouldn't be visible in the video device list
            StartCoroutine(CoroutineRefreshLater());
#endif
        }

        protected override void Awake()
        {
            base.Awake();

            spawnedObjects = new List<GameObject>();
            currentDrawingHits = new List<Vector3>();
            currentLineRenderers = new List<GameObject>();
            allDrawings = new List<LineRenderer>();
        }

        protected override void Update()
        {
            base.Update();

            if (Input.touchCount > 0)
            {
                currentDrawingMode = DrawingMode.DRAWING;

                //mUi.debugText.text += "Update()>Input.touchCount = " + Input.touchCount;
                Touch touch = Input.GetTouch(0);

                if (m_SessionOrigin.Raycast(touch.position, s_Hits, TrackableType.All))
                {
                    mUi.debugText.text += "Update()>Input.s_Hits.Count = " + s_Hits.Count + ", s_Hits[0].hitType = " + s_Hits[0].hitType + "\n";
                    if (currentDrawingHits.Count > 0)
                    {
                        currentLineRenderer = Instantiate(lineRenderer2p);
                        currentLineRenderers.Add(currentLineRenderer);
                        currentLineRenderer.GetComponent<LineRenderer>().SetPositions(
                            new Vector3[2] {
                        currentDrawingHits[currentDrawingHits.Count - 1],
                        s_Hits[0].pose.position });
                    }
                    currentDrawingHits.Add(s_Hits[0].pose.position);
                }
                else
                {
                    mUi.debugText.text += "Update()>Input.s_Hits.Count = 0";
                }
            }
            else
            {
                if (currentDrawingMode == DrawingMode.DRAWING)
                {
                    if (currentDrawingHits.Count == 1)
                    {//samo jedan touch => pin, instanciraj nesto - u zavisnosti od toolbara - kruzic ili strelica
                        Instantiate(m_PlacedPrefab, currentDrawingHits[0], Quaternion.identity);
                    }

                    if (currentDrawingHits.Count > 2)
                    {
                        Vector3[] smoothVectors = LineSmoother.SmoothLine(currentDrawingHits.ToArray<Vector3>(), 0.15f);
                        LineRenderer newLineRenderer = Instantiate(lineRenderer2p).GetComponent<LineRenderer>();
                        newLineRenderer.positionCount = smoothVectors.Length;
                        newLineRenderer.SetPositions(smoothVectors);
                        allDrawings.Add(newLineRenderer);//dodaje na kraj uvek jer bi trebalo da imamo UNDO
                        foreach (GameObject lr in currentLineRenderers) Destroy(lr);
                    }

                    currentDrawingHits = new List<Vector3>();
                    currentLineRenderers = new List<GameObject>();
                    currentDrawingMode = DrawingMode.NOTDRAWING;
                }
            }
        }
        IEnumerator CoroutineRefreshLater()
        {
            yield return new WaitForSecondsRealtime(1);
            mUi.UpdateVideoDropdown();
        }

        protected override void Call_CallEvent(object sender, CallEventArgs e)
        {
            bool callBase = true;
            switch (e.Type)
            {
                case CallEventType.Message:
                    MessageEventArgs args = e as MessageEventArgs;
                    callBase = MessageReceived(args.Content);
                    break;
            }
            if (callBase) base.Call_CallEvent(sender, e);
        }
        bool MessageReceived(string message)
        {
            mUi.debugText.text += "MessageReceived>message = " + message;
            if (string.IsNullOrEmpty(message) || message.Length <= 1)
                return true;
            //starting character will decide what is this message for
            char messageType = message[0];
            message = message.Substring(1, message.Length - 1);
            switch (messageType)
            {
                case 'c'://chat message
                    return true;
                case 'a'://action
                    InstantiateCube(StringToVector2(message));

                    //TODO: aktiviraj akciju crtanja kao gore za local

                    Vector2 positionOnScreen = StringToVector2(message);
                    if (m_SessionOrigin.Raycast(positionOnScreen, s_Hits, TrackableType.PlaneWithinPolygon))
                    {
                        Pose hitPose = s_Hits[0].pose;
                    }
                    return false;
                case 'e'://end of action
                    return false;
                default:
                    return true;
            }

        }
        void InstantiateCube(Vector2 position)
        {
            if (m_SessionOrigin.Raycast(position, s_Hits, TrackableType.PlaneWithinPolygon))
            {
                Pose hitPose = s_Hits[0].pose;

                spawnedObjects.Add(Instantiate(m_PlacedPrefab, hitPose.position, hitPose.rotation));
            }

        }
    }
}