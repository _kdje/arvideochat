﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;


public class LogInScene : MonoBehaviour {

    #region consts
    const string USERDATAKEY = "USERDATA";
    #endregion

    #region UI
    public GameObject SignedInInRoot;
    public GameObject NotSignedInRoot;
    public GameObject loadingPopup;

    public InputField signInEMailInputField;
    public InputField signInPasswordInputField;
    public InputField createProfileEMailInputField;
    public InputField createProfilePasswordInputField;

    public Text userEMailText;

    public Button SignInButton;
    public Button CreateProfileButton;
    public Button SkipButton;
    public Button SignOutButton;
    public Button ContinueButton;  

    public Text logText;
    #endregion

    #region state
    enum LogInState
    {
        NotSignedIn,
        SignedIn
    }
    LogInState currentLogInState;
    LogInState CurrentLogInState
    {
        get
        {
            return currentLogInState;
        }
        set
        {
            currentLogInState = value;
            switch (currentLogInState)
            {
                case LogInState.NotSignedIn:
                    SignedInInRoot.SetActive(false);
                    NotSignedInRoot.SetActive(true);
                    break;
                case LogInState.SignedIn:
                    SignedInInRoot.SetActive(true);
                    NotSignedInRoot.SetActive(false);
                    break;
            }

        }
    }
    #endregion

    #region user
    class User
    {
        public string eMail;
        public string password;
    }
    User CurretntUser
    {
        get
        {
            string userString = PlayerPrefs.GetString(USERDATAKEY);
            if (string.IsNullOrEmpty(userString)) return null;
            return JsonConvert.DeserializeObject<User>(userString);
        }
        set
        {
            PlayerPrefs.SetString(USERDATAKEY, JsonConvert.SerializeObject(value));
        }
    }
    #endregion

    private void Start()
    {
        User currentUser = CurretntUser;

        if (currentUser == null)
        {
            CurrentLogInState = LogInState.NotSignedIn;
        }
        else
        {
            userEMailText.text = currentUser.eMail;
            CurrentLogInState = LogInState.SignedIn;
        }

        SetButtons();
    }

    #region UI_actions
    void SetButtons()
    {
        SignInButton.onClick.AddListener(SignInButtonClicked);
        CreateProfileButton.onClick.AddListener(CreateProfileClicked);
        SkipButton.onClick.AddListener(SkipButtonClicked);
        SignOutButton.onClick.AddListener(SignOutButtonClicked);
        ContinueButton.onClick.AddListener(ContinueButtonClicked);
    }
    void SignInButtonClicked()
    {
        if (string.IsNullOrEmpty(signInEMailInputField.text) || string.IsNullOrEmpty(signInPasswordInputField.text))
        {
            Log("User eMail or password can't be empty");
            return;
        }
        ShowLoadingPopup(true);
        FirebaseController.Instance.CreateUserWithEmailAsync(signInEMailInputField.text, signInPasswordInputField.text);
    }
    void CreateProfileClicked()
    {
        if (string.IsNullOrEmpty(createProfileEMailInputField.text) || string.IsNullOrEmpty(createProfilePasswordInputField.text))
        {
            Log("User eMail or password can't be empty");
            return;
        }
        ShowLoadingPopup(true);
        FirebaseController.Instance.CreateUserWithEmailAsync(createProfileEMailInputField.text, createProfilePasswordInputField.text);
    }
    void SkipButtonClicked()
    {
        SceneManager.LoadScene("ARScene");
    }
    void SignOutButtonClicked()
    {
        CurretntUser = null;
        FirebaseController.Instance.SignOut();
    }
    void ContinueButtonClicked()
    {
        SceneManager.LoadScene("ARScene");
    }

    public void SetLoggedAfterCreaton()
    {
        CurrentLogInState = LogInState.SignedIn;
        userEMailText.text = createProfileEMailInputField.text;
        CurretntUser = new User { eMail = createProfileEMailInputField.text, password = createProfilePasswordInputField.text };
    }
    public void SetLoggedAfterSignIn()
    {
        CurrentLogInState = LogInState.SignedIn;
        userEMailText.text = signInEMailInputField.text;
        CurretntUser = new User { eMail = signInEMailInputField.text, password = signInPasswordInputField.text };
    }
    #endregion

    #region popup_hangling
    public void ShowLoadingPopup(bool flag)
    {
        loadingPopup.SetActive(flag);
    }
    #endregion

    #region logging
    readonly bool shouldLogOnScreen = true;
    readonly bool shouldLogInConsole = true;
    public void Log(string message)
    {
        if (shouldLogOnScreen)
        {
            logText.text += message + "\n";
        }
        if (shouldLogInConsole)
        {
            Debug.Log(message);
        }
    }
    #endregion

    #region singletone
    public static LogInScene Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }
    #endregion
}
