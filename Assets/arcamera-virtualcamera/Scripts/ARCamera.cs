﻿#if !UNITY_WEBGL || UNITY_EDITOR
using Byn.Media;
using Byn.Media.Native;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;
//using Vuforia;

public class ARCamera : MonoBehaviour {


    public Camera _Camera;
    private float mLastSample;

    private Texture2D mTexture;
    private RenderTexture mRtBuffer = null;
    
    public RawImage _DebugTarget = null;
    
    public readonly string _DeviceName = "ARCamera";

    public int _Fps = 30;//60;    
    public int _Width = 1280;//640;
    public int _Height = 720;//480;

    private string mUsedDeviceName;
    private byte[] mByteBuffer = null;
    
    private NativeVideoInput mVideoInput;

    private void Awake()
    {
        mUsedDeviceName = _DeviceName;
        mRtBuffer = new RenderTexture(_Width, _Height, 0, RenderTextureFormat.ARGB32);

        mRtBuffer.wrapMode = TextureWrapMode.Repeat;

        mTexture = new Texture2D(_Width, _Height, TextureFormat.ARGB32, false);
    }

    // Use this for initialization
    void Start()
    {
        mVideoInput = UnityCallFactory.Instance.VideoInput;
        mVideoInput.AddDevice(mUsedDeviceName, _Width, _Height, _Fps);
        
        //StartCoroutine(addSpheres());
    }

    private void OnDestroy()
    {
        Destroy(mRtBuffer);
        Destroy(mTexture);

        if (mVideoInput != null)
            mVideoInput.RemoveDevice(mUsedDeviceName);
    }


    void Update()
    {
        //ensure correct fps
        float deltaSample = 1.0f / _Fps;
        mLastSample += Time.deltaTime;
        if (mLastSample >= deltaSample)
        {
            mLastSample -= deltaSample;

            //backup the current configuration to restore it later
            var oldTargetTexture = _Camera.targetTexture;
            var oldActiveTexture = RenderTexture.active;

            //Set the buffer as target and render the view of the camera into it
            _Camera.targetTexture = mRtBuffer;
            _Camera.Render();


            RenderTexture.active = mRtBuffer;
            mTexture.ReadPixels(new Rect(0, 0, mRtBuffer.width, mRtBuffer.height), 0, 0, false);
            mTexture.Apply();

            //get the byte array. still looking for a way to reuse the current buffer
            //instead of allocating a new one all the time
            mByteBuffer = mTexture.GetRawTextureData();


            //update the internal WebRTC device
            mVideoInput.UpdateFrame(mUsedDeviceName, mByteBuffer, mTexture.width, mTexture.height, WebRtcCSharp.VideoType.kBGRA, 0, true);



            //reset the camera/active render texture  in case it is still used for other purposes
            _Camera.targetTexture = oldTargetTexture;
            RenderTexture.active = oldActiveTexture;

            //update debug output if available
            if (_DebugTarget != null)
                _DebugTarget.texture = mTexture;
        }
    }
    //public Text positionText;
    //public Text rotationText;
    //IEnumerator addSpheres()
    //{
    //    while (true)
    //    {
    //        positionText.text = "_Camera.transform.position(" + _Camera.transform.localPosition.x + ", " + _Camera.transform.localPosition.y + ", " + _Camera.transform.localPosition.z + ")";
    //        rotationText.text = "_Camera.transform.rotation(" + _Camera.transform.localRotation.x + ", " + _Camera.transform.localRotation.y + ", " + _Camera.transform.localRotation.z + ")";
    //        yield return new WaitForSeconds(1);
    //    }
    //}
}
#endif
