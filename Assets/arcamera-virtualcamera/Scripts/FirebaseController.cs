﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.DynamicLinks;
using Firebase.Auth;
using System.Threading.Tasks;


public class FirebaseController : MonoBehaviour {
    
    #region firebase_data
    DependencyStatus dependencyStatus = DependencyStatus.UnavailableOther;
    public bool firebaseInitialized = false;

    protected FirebaseAuth auth;
    protected FirebaseAuth otherAuth;
    protected Dictionary<string, FirebaseUser> userByAuth = new Dictionary<string, FirebaseUser>();
    // Options used to setup secondary authentication object:
    private AppOptions otherAuthOptions = new AppOptions
    {
        ApiKey = "",
        AppId = "",
        ProjectId = ""
    };
    // Flag set when a token is being fetched.  This is used to avoid printing the token
    // in IdTokenChanged() when the user presses the get token button.
    private bool fetchingToken = false;
    // Whether to sign in / link or reauthentication *and* fetch user profile data.
    protected bool signInAndFetchProfile = false;

    #endregion

    #region init_firebase
    public void Start()
    {
        DebugLog("FirebaseController.Start");
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError(
                  "Could not resolve all Firebase dependencies: " + dependencyStatus);
                DebugLog("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }
    void InitializeFirebase()
    {
        DebugLog("FirebaseController.InitializeFirebase");
        DynamicLinks.DynamicLinkReceived += OnDynamicLink;
        firebaseInitialized = true;

        DebugLog("Setting up Firebase Auth");
        auth = FirebaseAuth.DefaultInstance;
        auth.StateChanged += AuthStateChanged;
        auth.IdTokenChanged += IdTokenChanged;
        // Specify valid options to construct a secondary authentication object.
        if (otherAuthOptions != null &&
            !(String.IsNullOrEmpty(otherAuthOptions.ApiKey) ||
              String.IsNullOrEmpty(otherAuthOptions.AppId) ||
              String.IsNullOrEmpty(otherAuthOptions.ProjectId)))
        {
            try
            {
                otherAuth = FirebaseAuth.GetAuth(Firebase.FirebaseApp.Create(
                  otherAuthOptions, "Secondary"));
                otherAuth.StateChanged += AuthStateChanged;
                otherAuth.IdTokenChanged += IdTokenChanged;
            }
            catch (Exception)
            {
                DebugLog("ERROR: Failed to initialize secondary authentication object.");
            }
        }
        AuthStateChanged(this, null);
    }
    // Track state changes of the auth object.
    void AuthStateChanged(object sender, EventArgs eventArgs)
    {
        FirebaseAuth senderAuth = sender as FirebaseAuth;
        FirebaseUser user = null;
        if (senderAuth != null) userByAuth.TryGetValue(senderAuth.App.Name, out user);
        if (senderAuth == auth && senderAuth.CurrentUser != user)
        {
            bool signedIn = user != senderAuth.CurrentUser && senderAuth.CurrentUser != null;
            if (!signedIn && user != null)
            {
                DebugLog("Signed out " + user.UserId);
            }
            user = senderAuth.CurrentUser;
            userByAuth[senderAuth.App.Name] = user;
            if (signedIn)
            {
                DebugLog("Signed in " + user.UserId);
            }
        }
    }

    // Track ID token changes.
    void IdTokenChanged(object sender, System.EventArgs eventArgs)
    {
        FirebaseAuth senderAuth = sender as FirebaseAuth;
        if (senderAuth == auth && senderAuth.CurrentUser != null && !fetchingToken)
        {
            senderAuth.CurrentUser.TokenAsync(false).ContinueWith(
              task => DebugLog(String.Format("Token[0:8] = {0}", task.Result.Substring(0, 8))));
        }
    }
    #endregion

    #region dynamic_link
    void OnDynamicLink(object sender, EventArgs args)
    {
        DebugLog("OnDynamicLink called");
        ReceivedDynamicLinkEventArgs dynamicLinkEventArgs = args as ReceivedDynamicLinkEventArgs;

        string chatRoomName = dynamicLinkEventArgs.ReceivedDynamicLink.Url.AbsolutePath;
        if (!string.IsNullOrEmpty(chatRoomName))
        {
            DebugLog("OnDynamicLink chatRoomName = " + chatRoomName);
            StartCoroutine(CallJoinAfterSec(2, chatRoomName));
        }
    }
    IEnumerator CallJoinAfterSec(float seconds, string chatRoomName)
    {
        yield return new WaitForSeconds(seconds);  
        CallAppUi ui = GameObject.Find("CallApp").GetComponent<CallAppUi>();
        if (ui != null)
        {
            DebugLog("OnDynamicLink JoinButtonPressed calling");
            ui.uRoomNameInputField.text = chatRoomName;
            ui.JoinButtonPressed();
        }
    }
    #endregion

    #region signing_in/out
    // Sign-in with an email and password.
    public Task SigninWithEmailAsync(string email, string password)
    {
        DebugLog("Attempting to sign in as " + email);
        if (signInAndFetchProfile)
        {
            return auth.SignInAndRetrieveDataWithCredentialAsync(
              EmailAuthProvider.GetCredential(email, password)).ContinueWith(
                HandleSignInWithSignInResult);
        }
        else
        {
            return auth.SignInWithEmailAndPasswordAsync(email, password)
              .ContinueWith(HandleSignInWithUser);
        }
    }
    // Called when a sign-in with profile data completes.
    void HandleSignInWithSignInResult(Task<SignInResult> task)
    {
        LogInScene.Instance.ShowLoadingPopup(false);
        DebugLog("Signed in with sign in result");
        
    }
    // Called when a sign-in without fetching profile data completes.
    void HandleSignInWithUser(Task<FirebaseUser> task)
    {
        LogInScene.Instance.ShowLoadingPopup(false);
        DebugLog("Signed in with user");
    }
    // Sign out the current user.
    public void SignOut()
    {
        DebugLog("Signing out.");
        auth.SignOut();
    }
    #endregion

    #region creating_profile0
    // Create a user with the email and password.
    public Task CreateUserWithEmailAsync(string email, string password)
    {
        DebugLog("Attempting to create user " + email);
        return auth.CreateUserWithEmailAndPasswordAsync(email, password)
          .ContinueWith((task) => {
              LogInScene.Instance.ShowLoadingPopup(false);
              var user = task.Result;
              if (user.IsEmailVerified)
              {
                  DebugLog("User Created");
                  LogInScene.Instance.SetLoggedAfterCreaton();
              }
              else
              {
                  DebugLog("User Created");
              }
              return task;
          }).Unwrap();
    }
    #endregion

    #region logging
    void DebugLog(string message)
    {
        if (UnityEngine.SceneManagement.SceneManager.GetActiveScene().name == "LogInScene")
        {
            LogInScene.Instance.Log(message);
        }
        else
            Debug.Log(message);
    }
    #endregion

    #region singletone
    public static FirebaseController Instance { get; private set; }
    private void Awake()
    {
        Instance = this;
    }
    #endregion
}
