﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class WebCallApp : CallApp {

    public Text DebugText;

    private void Append(string txt)
    {
        base.Append(txt);
        DebugText.text += txt;
    }
}
